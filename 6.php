<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $enlaces=[
            [
                'url'=>'1.php',
                'clase'=>'caja1',
                'etiqueta'=>'Ejercicio 1',
            ],
            [
                'url'=>'2.php',
                'etiqueta'=>'Ejercicio 2',
            ],
        ];
        
        /* 
         * opcion 1
         */
        echo "<ul>";
        foreach ($enlaces as $enlace) {
            echo "<li ";
            if(isset($enlace['clase'])){
                echo 'class="' . $enlace['clase'] .'"';
            }
            echo ">";
            echo '<a href="'. $enlace['url'] .'">'.$enlace['etiqueta'] .'</a>';
            echo "</li>";
        }
        echo "</ul>";
        
        /**
         * opcion 2
         */
        ?>
        
        <ul>
        <?php
            foreach ($enlaces as $enlace) {
                $clase="";
            if(isset($enlace["clase"])){
                $clase='class="' . $enlace["clase"] . '"';
            }
        ?>
            <li <?= $clase ?>>
                <a href="<?= $enlace["url"] ?>">
                    <?= $enlace["etiqueta"] ?>
                </a>
            </li>
            <?php
            }
            ?>
        </ul>
        ----
        <?php
        /*
         * opcion 3
         */
        
        echo "<ul>";
        foreach ($enlaces as $enlace) {
            if(isset($enlace["clase"])){
                echo '<li class="'. $enlace["clase"] .'">';
            }else{
                echo '<li>';
            }
            
            echo '<a href="'. 
                    $enlace["url"] .
                    '">'. 
                    $enlace["etiqueta"].
                    '</a>';
            echo "</li>";
    
        }
        echo "</ul>";
        ?>
        
        
        
        
        
        
        
    </body>
</html>
